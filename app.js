//Express Setup
const express = require('express');
const mongoose = require('mongoose');

//Allows our back end application to be availbale for use in frontend application
//Allows us to control the app's___________
const cors = require('cors')

const app = express();
const port = 4000;

//Middlewares:
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Mongoose Connection

mongoose.connect(`mongodb+srv://jessielibita:admin123@zuittbatch197cluster.gq4lrrn.mongodb.net/s37-s41?retryWrites=true&w=majority`,{
	useNewURLParser: true,
	useUnifiedTopology: true
})

const db = mongoose.connection

db.on ('error', () => console.error ('Connection Error'));
db.once('open', () => console.log ('Connected to MongoDB!'))

app.listen(port, () => console.log (`API is now online at port: ${port}`));