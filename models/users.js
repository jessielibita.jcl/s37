const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "Plese provide your First name"]
	},
	
	lastName: {
		type: String,
		required: [true, "Plese provide your last name"]
	},
	
	email: {
		type: String,
		required: [true, "Plese provide your last name"]
	},
	
	password: {
		type: String,
		required: [true, "Enter your password"]
	},
	
	isAdmin:{
		type: Boolean,
		default: false
	},
	
	mobileNo: {
		type: Number,
		required: [true, "Please provide a contact number."]
	},
	
	enrolledOn: {
		type: Date,
		default: new Date()
	},

	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "Enter a valid course ID"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Enrolled"
			}
		}
	]

})

module.export = mongoose.model("Users", userSchema);